import requests
import helpers

def __get_filtered_posts():
	url = 'https://api.producthunt.com/v1/posts'
	params = { 'day': '2015-12-02' }
	headers = { 'Authorization' : 'Bearer 6d552893f3b9854990e72be247c466714e1e00251ee4cedb87a743d46ce28a91' }

	response = requests.get(url, params = params, headers = headers)
	posts = response.json()['posts']
	
	return helpers.filter_dict(response.json()['posts'], ['id', 'votes_count'])

def get_best_post():
	filtered_posts = __get_filtered_posts()
	sorted_posts = sorted(filtered_posts, key = lambda k: k['votes_count'], reverse = True)
	
	return sorted_posts[0]